<!--
 Copyright (c) 2021 Space Rock Media LLC.

 This software is released under the MIT License.
 https://opensource.org/licenses/MIT
-->

Space Rock Media Docker containers
---

This is a spot for my current docker containers and dockerfile references for ease of use in a local environment.

# images used in production

## images/python (based on slim-buster)
my python images are mostly base images with a few packages installed to run a default app with gunicorn. May be changed to uvicorn or others in the future. Current supplied versions are:

* 3.9
[![pipeline status](https://gitlab.com/SpaceRockMedia/containers/badges/3.9-slim-buster/pipeline.svg)](https://gitlab.com/SpaceRockMedia/containers/-/commits/3.9-slim-buster)
* 3.10
[![pipeline status](https://gitlab.com/SpaceRockMedia/containers/badges/3.10-slim-buster/pipeline.svg)](https://gitlab.com/SpaceRockMedia/containers/-/commits/3.10-slim-buster)

# images only tested in local dev

## impages/pgadmin4
Just a simple configuration to get pgadmin4 up and running locally.

## impages/portainer
Just a simple configuration to get portainer up and running locally.

## impages/postgres
Just a simple configuration to get postgres up and running locally.

## impages/traefik
Still in development, but this is to serve public pages over https on port 80, and to route traffic to containers based on container labels and hostnames.

# CI
You can see the CI jobs in `.gitlab/.gitlab-ci.yml`. When possible, containers are pushed to the gitlab registry when a tag is pushed to the repository matching `[image]/$CI_COMMIT_TAG`. For example, if I update the python 3.9 image, and update the `3.9-slim-buster` tag, it will trigger only when files are changed in the directory `images/python/$CI_COMMIT_TAG`, or in this case `images/python/3.9-slim-buster`.
