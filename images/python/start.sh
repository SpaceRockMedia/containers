#!/usr/bin/env sh

set -ex

export env="prod"
/venv/bin/gunicorn \
  -k uvicorn.workers.UvicornWorker \
  -c /app/environments/gunicorn_conf.py \
  "${app_module:-'src/bo/entrypoints/fastapi_app:app'}"
